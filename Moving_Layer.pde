class Moving_Layer {

  float x_position = 0;
  float y_position = 0;
  float radius = 3;
  int lightintensity = 0;
  boolean visible = false;

  //Constructor

  Moving_Layer(float _x, float _y) {
    x_position = _x;
    y_position = _y;
  }



  void draw_layer_point() {
    pushMatrix();
    if(!visible){
    noStroke();
    noFill();
    }
    if(visible){
    fill(0,255,255);
    }
      ellipse(x_position, y_position, radius, radius);
    popMatrix();
  }

  void translate_to(float _x, float _y) {
    x_position = _x;
    y_position = _y;
    return;
  }

  void move(float _x, float _y) {
      x_position = x_position+_x;
      y_position = y_position+_y;
      return;
    }
}
