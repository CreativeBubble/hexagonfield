ArrayList <Hexagon> hexagon_field = new ArrayList<Hexagon>();
ArrayList <Moving_Layer> layer_which_moves_over_field = new ArrayList<Moving_Layer>();
ArrayList <Hex> field_with_old_hex_values = new ArrayList<Hex>();
boolean vertical_movement = false;
boolean horizontal_movement = false;
boolean diagonal_movement = true;

PrintWriter output;


void setup() { //<>// //<>// //<>// //<>// //<>// //<>// //<>//
  size(500, 400);
  background(25);
  output = createWriter("vertical_movement.h");

  fill_hex_array();
  create_hexagon_field();
  change_ids(); //<>// //<>// //<>// //<>// //<>// //<>// //<>// //<>// //<>// //<>// //<>//
  if(vertical_movement){fill_layer_array_vertical();}    //for movement from right to left
  if(horizontal_movement){fill_layer_array_horizontal();}    //for movement from up to down
  if(diagonal_movement){fill_layer_array_diagonal();}    //for movement diagonal
  draw_layer();
  write_struct_headers();


}


void draw() {
  background(25);
  draw_hexagons();

  if(vertical_movement){move_layer_from_right_to_left();}
  if(horizontal_movement){move_layer_from_up_to_down();}
  if(diagonal_movement){move_layer_diagonal();}

  draw_layer();
  check_near_to_layer();
  write_struct();
  write_struct_ending();

}

void write_struct_headers(){
  output.println("struct vertical_movement {");
  output.println("       byte id = 0;");
  output.println("       byte intensity 0;");
  output.println("  };");
  output.println();
  output.println("vertical_movement [] = { ");
}

void write_struct(){
  for(Hexagon actual_hexagon : hexagon_field){
    for(Hex old_hex : field_with_old_hex_values){
      if(actual_hexagon.id == old_hex.id){
        if(actual_hexagon.lightintensity != old_hex.lightintensity){
          old_hex.lightintensity = actual_hexagon.lightintensity;
          if(actual_hexagon.id != 0){
            output.println("                    {"+actual_hexagon.id + ", "+ actual_hexagon.lightintensity +"},");
          }

        }
      }
    }

  }
}

void write_struct_ending(){
  if(frameCount == 200){  output.println("};");
                          output.flush();
                          output.close();
                         // exit();
                       }
}

void check_near_to_layer() {
  for (Hexagon actual_hexagon : hexagon_field) {
    int counter = layer_which_moves_over_field.size();
    for (Moving_Layer actual_layer_point : layer_which_moves_over_field) {
      if (dist(actual_hexagon.x_position, actual_hexagon.y_position,
        actual_layer_point.x_position, actual_layer_point.y_position)<80)
      {
        actual_hexagon.is_visible = true;
        actual_hexagon.increase_lightintensity();
      }
      else
      {
        counter = counter-1;
      }
    }
    if(counter == 0){
      actual_hexagon.decrease_lightintensity();
    //  actual_hexagon.is_visible = false;
    }
  }
}

void fill_hex_array() {
  for (int i=0; i<54; i++) {
    Hexagon new_Hexagon = new Hexagon(i+50);
    hexagon_field.add(new_Hexagon);
    Hex new_Hex = new Hex(i+2);
    field_with_old_hex_values.add(new_Hex);


  }
}

void create_hexagon_field() {
  int counter = 0;
  for (int l = 0; l<9; l++) {
    for (int i = 0; i < 6; i++) {
      if (l%2==0) {
        Hexagon hex = hexagon_field.get(counter);
        hex.translate((60+l*43), (40+i*45));
      }
      if (l%2!=0) {
        Hexagon hex = hexagon_field.get(counter);
        hex.translate((60+l*43), (65+i*45));
      }
      counter = counter +1;
    }
  }
  return;
}

void change_ids() {
  for (int i = 0; i < hexagon_field.size(); i++) {
    //  Hexagon hex = hexagon_field.get(i);
    if (hexagon_field.get(i).id == 50) {
      hexagon_field.get(i).id = 0;
    }
    if (hexagon_field.get(i).id == 51) {
      hexagon_field.get(i).id = 0;
    }
    if (hexagon_field.get(i).id == 52) {
      hexagon_field.get(i).id = 0;
    }
    if (hexagon_field.get(i).id == 53) {
      hexagon_field.get(i).id = 31;
    }
    if (hexagon_field.get(i).id == 54) {
      hexagon_field.get(i).id = 32;
    }
    if (hexagon_field.get(i).id == 55) {
      hexagon_field.get(i).id = 0;
    }
    if (hexagon_field.get(i).id == 56) {
      hexagon_field.get(i).id = 0;
    }
    if (hexagon_field.get(i).id == 57) {
      hexagon_field.get(i).id = 26;
    }
    if (hexagon_field.get(i).id == 58) {
      hexagon_field.get(i).id = 27;
    }
    if (hexagon_field.get(i).id == 59) {
      hexagon_field.get(i).id = 28;
    }
    if (hexagon_field.get(i).id == 60) {
      hexagon_field.get(i).id = 29;
    }
    if (hexagon_field.get(i).id == 61) {
      hexagon_field.get(i).id = 30;
    }
    if (hexagon_field.get(i).id == 62) {
      hexagon_field.get(i).id = 0;
    }
    if (hexagon_field.get(i).id == 63) {
      hexagon_field.get(i).id = 0;
    }
    if (hexagon_field.get(i).id == 64) {
      hexagon_field.get(i).id = 22;
    }
    if (hexagon_field.get(i).id == 65) {
      hexagon_field.get(i).id = 23;
    }
    if (hexagon_field.get(i).id == 66) {
      hexagon_field.get(i).id = 24;
    }
    if (hexagon_field.get(i).id == 67) {
      hexagon_field.get(i).id = 25;
    }
    if (hexagon_field.get(i).id == 68) {
      hexagon_field.get(i).id = 0;
    }
    if (hexagon_field.get(i).id == 69) {
      hexagon_field.get(i).id = 17;
    }
    if (hexagon_field.get(i).id == 70) {
      hexagon_field.get(i).id = 18;
    }
    if (hexagon_field.get(i).id == 71) {
      hexagon_field.get(i).id = 19;
    }
    if (hexagon_field.get(i).id == 72) {
      hexagon_field.get(i).id = 20;
    }
    if (hexagon_field.get(i).id == 73) {
      hexagon_field.get(i).id = 21;
    }
    if (hexagon_field.get(i).id == 74) {
      hexagon_field.get(i).id = 0;
    }
    if (hexagon_field.get(i).id == 75) {
      hexagon_field.get(i).id = 12;
    }
    if (hexagon_field.get(i).id == 76) {
      hexagon_field.get(i).id = 13;
    }
    if (hexagon_field.get(i).id == 77) {
      hexagon_field.get(i).id = 14;
    }
    if (hexagon_field.get(i).id == 78) {
      hexagon_field.get(i).id = 15;
    }
    if (hexagon_field.get(i).id == 79) {
      hexagon_field.get(i).id = 16;
    }
    if (hexagon_field.get(i).id == 80) {
      hexagon_field.get(i).id = 0;
    }
    if (hexagon_field.get(i).id == 81) {
      hexagon_field.get(i).id = 9;
    }
    if (hexagon_field.get(i).id == 82) {
      hexagon_field.get(i).id = 10;
    }
    if (hexagon_field.get(i).id == 83) {
      hexagon_field.get(i).id = 11;
    }
    if (hexagon_field.get(i).id == 84) {
      hexagon_field.get(i).id = 0;
    }
    if (hexagon_field.get(i).id == 85) {
      hexagon_field.get(i).id = 0;
    }
    if (hexagon_field.get(i).id == 86) {
      hexagon_field.get(i).id = 0;
    }
    if (hexagon_field.get(i).id == 87) {
      hexagon_field.get(i).id = 0;
    }
    if (hexagon_field.get(i).id == 88) {
      hexagon_field.get(i).id = 6;
    }
    if (hexagon_field.get(i).id == 89) {
      hexagon_field.get(i).id = 7;
    }
    if (hexagon_field.get(i).id == 90) {
      hexagon_field.get(i).id = 8;
    }
    if (hexagon_field.get(i).id == 91) {
      hexagon_field.get(i).id = 0;
    }
    if (hexagon_field.get(i).id == 92) {
      hexagon_field.get(i).id = 0;
    }
    if (hexagon_field.get(i).id == 93) {
      hexagon_field.get(i).id = 3;
    }
    if (hexagon_field.get(i).id == 94) {
      hexagon_field.get(i).id = 4;
    }
    if (hexagon_field.get(i).id == 95) {
      hexagon_field.get(i).id = 5;
    }
    if (hexagon_field.get(i).id == 96) {
      hexagon_field.get(i).id = 0;
    }
    if (hexagon_field.get(i).id == 97) {
      hexagon_field.get(i).id = 0;
    }
    if (hexagon_field.get(i).id == 98) {
      hexagon_field.get(i).id = 0;
    }
    if (hexagon_field.get(i).id == 99) {
      hexagon_field.get(i).id = 0;
    }
    if (hexagon_field.get(i).id == 100) {
      hexagon_field.get(i).id = 2;
    }
    if (hexagon_field.get(i).id == 101) {
      hexagon_field.get(i).id = 0;
    }
    if (hexagon_field.get(i).id == 102) {
      hexagon_field.get(i).id = 0;
    }
    if (hexagon_field.get(i).id == 103) {
      hexagon_field.get(i).id = 0;
    }
  }
}

void draw_hexagons() {
  for (int i = 0; i < hexagon_field.size(); i++) {
    if (hexagon_field.get(i).id == 0) {
      hexagon_field.get(i).is_visible = false;
    }
    if (hexagon_field.get(i).is_visible) {
      hexagon_field.get(i).draw_hexagon();
    }
  }
  return;
}

void fill_layer_array_vertical() {
  for (int i = 0; i< 30; i++) {
    Moving_Layer new_layer_point = new Moving_Layer(width, i*20);
    layer_which_moves_over_field.add(new_layer_point);
  }
}
void fill_layer_array_horizontal() {
  for (int i = 0; i< 30; i++) {
    Moving_Layer new_layer_point = new Moving_Layer(i*20, 0);
    layer_which_moves_over_field.add(new_layer_point);
  }
}
void fill_layer_array_diagonal() {
  for (int i = 0; i< 30; i++) {
    Moving_Layer new_layer_point = new Moving_Layer(width+i*15, i*20);
    layer_which_moves_over_field.add(new_layer_point);
  }
}

void draw_layer() {
  for (Moving_Layer actual_layer : layer_which_moves_over_field) {
    actual_layer.draw_layer_point();
  }
}

void move_layer_diagonal() {
  for (Moving_Layer actual_layer : layer_which_moves_over_field) {
    if (actual_layer.x_position<0) {
      actual_layer.translate_to(width, actual_layer.y_position);
    }
    actual_layer.move(-4, 0);
  }
}

void move_layer_from_right_to_left() {
  for (Moving_Layer actual_layer : layer_which_moves_over_field) {
    if (actual_layer.x_position<0) {
      actual_layer.translate_to(width, actual_layer.y_position);
    }
    actual_layer.move(-2, 0);
  }
}

void move_layer_from_up_to_down() {
  for (Moving_Layer actual_layer : layer_which_moves_over_field) {
    if (actual_layer.y_position<0) {
      actual_layer.translate_to(actual_layer.x_position, height);
    }
    actual_layer.move(0, -3);
  }
}
